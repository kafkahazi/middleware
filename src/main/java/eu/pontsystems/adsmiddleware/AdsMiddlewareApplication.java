package eu.pontsystems.adsmiddleware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdsMiddlewareApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdsMiddlewareApplication.class, args);
    }

}
