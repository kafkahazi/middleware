package eu.pontsystems.adsmiddleware.controller;

import eu.pontsystems.adsmiddleware.constants.KafkaConstants;
import eu.pontsystems.adsmiddleware.model.Advertisement;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
@AllArgsConstructor
public class AdvertisementController {

    private final KafkaTemplate<String, Advertisement> kafkaTemplate;

    @PostMapping(value = "/middleware/kafka/send", consumes = "application/json", produces = "application/json")
    public void sendMessage(@RequestBody Advertisement advertisement) {
        try {
            kafkaTemplate.send(KafkaConstants.KAFKA_TOPIC, advertisement).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

}
