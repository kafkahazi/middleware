package eu.pontsystems.adsmiddleware.model;

import lombok.Data;

import java.util.UUID;

@Data
public class Advertisement {

    private UUID id = UUID.randomUUID();
    private String name;
    private String message;
}
